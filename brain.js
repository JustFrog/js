// Модуль 6
// Задание 1
let objects = [
	{
		name: 'Василий',
		surname: 'Васильев'
	},
	{
		name: 'Иван',
		surname: 'Иванов'
	},
	{
		name: 'Пётр',
		surname: 'Петров'
	},
];

function sortingArray(objects) {
	let array = [];
	let obj = Object.entries(objects);
	for (let i = obj.length - 1; i >= 0; i--) {
		array.push(obj[i]);
	}
	return array;
}

console.log(sortingArray(objects));

// Задание 2
